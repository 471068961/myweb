package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()

	// 加载静态文件
	r.Static("css", "../css")
	// r.Static("css/img", "../css/img")
	r.Static("fa", "../fa")

	// 解析主页
	r.LoadHTMLFiles("../index.html", "../html/login.html", "../html/loginfiled.html")

	r.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", nil)
	})
	r.GET("/login", func(c *gin.Context) {
		c.HTML(http.StatusOK, "login.html", nil)
	})

	r.POST("/login", func(c *gin.Context) {
		username := c.PostForm("username")
		password := c.PostForm("password")
		if username == "admin" && password == "123456" {
			c.HTML(http.StatusOK, "index.html", nil)
		} else {
			c.HTML(http.StatusOK, "loginfiled.html", nil)
		}
	})
	r.Run(":9090")
}
